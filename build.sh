#!/bin/bash

set -e

select_multiarchname()
{
    echo "Please select the desired architecture:"
    echo "1): i386-linux-gnu"
    echo "2): x86_64-linux-gnu"
    echo "3): armeabi-linux-android"
    echo "4): armeabi_v7a-linux-android"
    echo "5): i386-darwin-macos"
    echo "6): x86_64-darwin-macos"
    echo "7): universal-darwin-macos (fat binary i386 and x86_64)"
    read -r SELECTEDOPTION
    if [ x"$SELECTEDOPTION" = x"1" ]; then
        MULTIARCHNAME=i386-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"2" ]; then
        MULTIARCHNAME=x86_64-linux-gnu
    elif [ x"$SELECTEDOPTION" = x"3" ]; then
        MULTIARCHNAME=armeabi-linux-android
    elif [ x"$SELECTEDOPTION" = x"4" ]; then
        MULTIARCHNAME=armeabi_v7a-linux-android
    elif [ x"$SELECTEDOPTION" = x"5" ]; then
        MULTIARCHNAME=i386-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"6" ]; then
        MULTIARCHNAME=x86_64-darwin-macos
    elif [ x"$SELECTEDOPTION" = x"7" ]; then
        MULTIARCHNAME=universal-darwin-macos
    else
        echo "Invalid option selected!"
        select_multiarchname
    fi
}

linux_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        BUILDTYPE=Debug
    else
        BUILDTYPE=Release
    fi

    if [ x"$OPTIMIZATION" = xlow ]; then
        echo "Optimization level \"low\" is not yet implemented."
        exit
    fi

    rm -rf "$PREFIXDIR"

    mkdir "$BUILDDIR"
    ( cd "$BUILDDIR"
    cmake ../source \
        -DCMAKE_CXX_FLAGS="$OPTIMIZATION" \
        -DCMAKE_C_FLAGS="$OPTIMIZATION" \
        -DPHYSFS_ARCHIVE_7Z=FALSE \
        -DPHYSFS_ARCHIVE_GRP=FALSE \
        -DPHYSFS_ARCHIVE_HOG=FALSE \
        -DPHYSFS_ARCHIVE_ISO9660=FALSE \
        -DPHYSFS_ARCHIVE_MVL=FALSE \
        -DPHYSFS_ARCHIVE_QPAK=FALSE \
        -DPHYSFS_ARCHIVE_SLB=FALSE \
        -DPHYSFS_ARCHIVE_VDF=FALSE \
        -DPHYSFS_ARCHIVE_WAD=FALSE \
        -DPHYSFS_ARCHIVE_ZIP=TRUE \
        -DPHYSFS_BUILD_SHARED=TRUE \
        -DPHYSFS_BUILD_STATIC=FALSE \
        -DCMAKE_BUILD_TYPE=$BUILDTYPE \
        -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
        -DPHYSFS_BUILD_TEST=FALSE
    make
    make install
    rm -rf "$PREFIXDIR"/lib/pkgconfig )
}

darwin_build()
{
    if [ x"$BUILDTYPE" = xdebug ]; then
        BUILDTYPE=Debug
    else
        BUILDTYPE=Release
    fi

    if [ x"$OPTIMIZATION" = xlow ]; then
        echo "Optimization level \"low\" is not yet implemented."
        exit
    fi

    case "$MULTIARCHNAME" in
    i386-darwin-macos)
        BUILDARCH="i386"
        ;;
    x86_64-darwin-macos)
        BUILDARCH="x86_64"
        ;;
    universal-darwin-macos)
        BUILDARCH="x86_64;i386"
        ;;
    *)
        echo "$MULTIARCHNAME is not (yet) supported by this script."
        exit 1;;
    esac

    mkdir "$BUILDDIR"
    ( cd "$BUILDDIR"
    cmake ../source \
        -DCMAKE_CXX_FLAGS="$OPTIMIZATION" \
        -DCMAKE_C_FLAGS="$OPTIMIZATION" \
        -DPHYSFS_ARCHIVE_7Z=FALSE \
        -DPHYSFS_ARCHIVE_GRP=FALSE \
        -DPHYSFS_ARCHIVE_HOG=FALSE \
        -DPHYSFS_ARCHIVE_ISO9660=FALSE \
        -DPHYSFS_ARCHIVE_MVL=FALSE \
        -DPHYSFS_ARCHIVE_QPAK=FALSE \
        -DPHYSFS_ARCHIVE_SLB=FALSE \
        -DPHYSFS_ARCHIVE_WAD=FALSE \
        -DPHYSFS_ARCHIVE_ZIP=TRUE \
        -DPHYSFS_BUILD_STATIC=FALSE \
        -DPHYSFS_BUILD_SHARED=TRUE \
        -DMACOSX_RPATH=TRUE \
        -DCMAKE_BUILD_TYPE=$BUILDTYPE \
        -DCMAKE_INSTALL_PREFIX="$PREFIXDIR" \
        -DPHYSFS_BUILD_TEST=FALSE \
        -DCMAKE_OSX_ARCHITECTURES="$BUILDARCH"
    make
    make install 
    install_name_tool -id @executable_path/../Frameworks/libphysfs.2.1.0.dylib "$PREFIXDIR"/lib/libphysfs.2.1.0.dylib
    )
}


if [ -z "$MULTIARCHNAME" ]; then
    echo "\$MULTIARCHNAME is not set!"
    select_multiarchname
fi

if [ -z "$BUILDDIR" ]; then
    BUILDDIR="build_$MULTIARCHNAME"
fi

if [ -z "$PREFIXDIR" ]; then
    PREFIXDIR="$PWD/$MULTIARCHNAME"
fi

case "$MULTIARCHNAME" in
i386-linux-gnu)
    linux_build;;
x86_64-linux-gnu)
    linux_build;;
*-linux-android)
    echo "Please use the supplied Android.mk file instead of this script."
    exit 1;;
i386-darwin-macos)
    darwin_build;;
x86_64-darwin-macos)
    darwin_build;;
universal-darwin-macos)
    darwin_build;;
*)
    echo "$MULTIARCHNAME is not (yet) supported by this script."
    exit 1;;
esac

install -m664 source/LICENSE.txt "$PREFIXDIR"/lib/LICENSE-physfs.txt

rm -rf "$BUILDDIR"

echo "PhysicsFS for $MULTIARCHNAME is ready."
