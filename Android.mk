LOCAL_PATH := $(call my-dir)

###########################
#
# PhysFS shared library
#
###########################

include $(CLEAR_VARS)

LOCAL_MODULE := PhysFS

LOCAL_C_INCLUDES := $(LOCAL_PATH)/source/src

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_C_INCLUDES)

LOCAL_SRC_FILES := \
	$(subst $(LOCAL_PATH)/,, \
	$(wildcard $(LOCAL_PATH)/source/src/*.c) )

LOCAL_CFLAGS += \
    -Werror \
    -fsigned-char \
    -fvisibility=hidden \
    -Wall \
    -D_REENTRANT \
    -D_THREAD_SAFE \
    -DPHYSFS_SUPPORTS_ZIP=1 \
    -DPHYSFS_SUPPORTS_APK=1 \
    -Wno-maybe-uninitialized #GCC 4.0 on Android L produces an error I do not
                             #understand and which looks pretty faulty.

LOCAL_LDLIBS := -ldl -llog

include $(BUILD_SHARED_LIBRARY)
