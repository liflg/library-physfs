Website
=======
https://icculus.org/physfs/

License
=======
zlib license (see the file source/LICENSE.txt)

Version
=======
3.0.1

Source
======
physfs-3.0.1.tar.bz2 (sha256: b77b9f853168d9636a44f75fca372b363106f52d789d18a2f776397bf117f2f1)

Required by
===========
* physfs-cpp
